import time
import logging
import scrapy


class NewsSpider(scrapy.Spider):
    name = "news"
    allowed_domain = ["detik.com"]
    start_urls = ["https://news.detik.com/indeks?date=08%2F23%2F2020"]

    def parse(self, response):
        logging.info(f"URL: {response.url}")
        titles = response.xpath(
            '//article//h3[@class="media__title"]/a/@href'
        ).extract()
        for url in titles:
            yield scrapy.Request(url, callback=self.parse_news)

        next_button = response.xpath('//a[@class="pagination__item"]')[-1]
        onclick_attr = next_button.xpath("@onclick").extract()

        if onclick_attr:
            next_url = next_button.xpath("@href").extract()[0]
            time.sleep(5)
            yield scrapy.Request(next_url)

    def parse_news(self, response):
        title = (
            response.xpath('//h1[@class="detail__title"]/text()').get().strip()
        )
        author = (
            response.xpath('//div[@class="detail__author"]/text()')
            .get()
            .split("-")[0]
            .strip()
        )
        release_date = response.xpath(
            '//div[@class="detail__date"]/text()'
        ).get()
        image = response.xpath('//div[@class="detail__media"]//img/@src').get()
        body = response.xpath('//div[@class="detail__body-text"]').get()

        yield {
            "title": title,
            "author": author,
            "date": release_date,
            "image": image,
            "body": body,
        }
